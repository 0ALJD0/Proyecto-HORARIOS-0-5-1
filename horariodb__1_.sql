-- phpMyAdmin SQL Dump
-- version 5.2.0-dev+20211216.1258178f68
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 02, 2022 at 07:21 PM
-- Server version: 8.0.27
-- PHP Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `horariodb`
--

-- --------------------------------------------------------

--
-- Table structure for table `horario`
--

CREATE TABLE `horario` (
  `id_horario` int NOT NULL,
  `id_materia` int NOT NULL,
  `dia_s` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `hora_ini` time DEFAULT NULL,
  `hora_fin` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `horario`
--

INSERT INTO `horario` (`id_horario`, `id_materia`, `dia_s`, `hora_ini`, `hora_fin`) VALUES
(1, 1, 'LUNES, MARTES, MIERCOLES', '08:00:00', '10:00:00'),
(2, 1, 'MIERCOLES, JUEVES, VIERNES', '15:00:00', '17:00:00'),
(3, 2, 'LUNES, MARTES', '10:00:00', '12:00:00'),
(4, 2, 'LUNES, MARTES', '15:00:00', '17:00:00'),
(5, 3, 'MIERCOLES', '10:00:00', '12:00:00'),
(6, 3, 'MIERCOLES', '17:00:00', '19:00:00'),
(7, 4, 'JUEVES, VIERNES', '08:00:00', '10:00:00'),
(8, 4, 'JUEVES, VIERNES', '17:00:00', '19:00:00'),
(9, 5, 'LUNES, MARTES, MIERCOLES', '08:00:00', '10:00:00'),
(10, 5, 'MIERCOLES, JUEVES, VIERNES', '15:00:00', '17:00:00'),
(11, 6, 'LUNES, MARTES', '10:00:00', '12:00:00'),
(12, 6, 'LUNES, MARTES', '15:00:00', '17:00:00'),
(13, 7, 'MIERCOLES', '10:00:00', '12:00:00'),
(14, 7, 'MIERCOLES', '17:00:00', '19:00:00'),
(15, 8, 'JUEVES, VIERNES', '08:00:00', '10:00:00'),
(16, 8, 'JUEVES, VIERNES', '17:00:00', '19:00:00'),
(17, 4, 'Lunes', '08:00:00', '10:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `horario_asignado`
--

CREATE TABLE `horario_asignado` (
  `id_horario` int NOT NULL,
  `id_usuario` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `materia`
--

CREATE TABLE `materia` (
  `id_materia` int NOT NULL,
  `id_nivel` int NOT NULL,
  `nomb_materia` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `materia`
--

INSERT INTO `materia` (`id_materia`, `id_nivel`, `nomb_materia`) VALUES
(1, 1, 'FÍSICA'),
(2, 1, 'ALGEBRA'),
(3, 1, 'FUNDAMENTOS DE PROGRAMACIÓN'),
(4, 1, 'FUNDAMENTOS DE TI'),
(5, 2, 'ESTRUCTURA DE DATOS'),
(6, 2, 'SISTEMAS ELECTRÍCOS'),
(7, 2, 'CÁLCULO DE UNA VARIABLE'),
(8, 2, 'MATEMÁTICAS DDISCRETAS');

-- --------------------------------------------------------

--
-- Table structure for table `nivel`
--

CREATE TABLE `nivel` (
  `id_nivel` int NOT NULL,
  `nomb_nivel` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `nivel`
--

INSERT INTO `nivel` (`id_nivel`, `nomb_nivel`) VALUES
(1, 'PRIMER NIVEL'),
(2, 'SEGUNDO NIVEL');

-- --------------------------------------------------------

--
-- Table structure for table `paralelo`
--

CREATE TABLE `paralelo` (
  `id_paralelo` int NOT NULL,
  `id_nivel` int NOT NULL,
  `paralelo` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `paralelo`
--

INSERT INTO `paralelo` (`id_paralelo`, `id_nivel`, `paralelo`) VALUES
(1,1,'A'),
(2,1,'B'),
(3,2,'A'),
(4,2,'B');

-- --------------------------------------------------------

--
-- Table structure for table `rol`
--

CREATE TABLE `rol` (
  `id_rol` int NOT NULL,
  `nombre_rol` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `rol`
--

INSERT INTO `rol` (`id_rol`, `nombre_rol`) VALUES
(1, 'Estudiante'),
(2, 'Docente'),
(3, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int NOT NULL,
  `id_rol` int DEFAULT NULL,
  `nombre` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellido` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `nombre_usuario` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `correo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `contrasena` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `id_rol`, `nombre`, `apellido`, `nombre_usuario`, `correo`, `contrasena`, `direccion`) VALUES
(1, 1,'FERNANDO','CARDENAS', 'fercar','fcar@gmail.com', 'Estud2021', 'COSTA AZUL'),
(2,2,'TATIANA','MACÍAS','tatmac', 'tmac@gmail.com','Prof2021','LOS ESTEROS'),
(3,1,'JAIME','DUARTE', 'jaidua', 'jdua@gmail.com', 'Estud2021','COSTA AZUL'),
(4,2,'MONSERRATE','LUCAS','monluc', 'mluc@gmail.com','Prof2021','LOS ESTEROS'),
(5,1,'PETER','LOPEZ', 'petlop', 'bloo@gmail.com','Estud2021','LA AURORA'),
(6,1,'MARIA','ORDOÑEZ','marord','mord@gmail.com','Estud2021','LAS MARIAS'),
(7,1,'FERNANDO','FLORES','fernanfloo','fflo@gmail.com','Estud2021','JOCAY'),
(8,1,'MARIA','GUERRERO','margue','mgue@gmail.com','Estud2021','LAS CUMBRES'),
(9,2,'DIEGO','CHICA','diechi','dchi@gmail.com','Prof2021','15 DE SEPTIEMBRE'),
(10,1,'MARCELA','CARDONA', 'marcar','mcarD@gmail.com','Estud2021','CORDOVA'),
(11,3,'KEVIN','PONCE', 'kevpon','kponce@gmail.com','Admin2021','JOCKAY');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`id_horario`),
  ADD KEY `FK_RELATIONSHIP_3` (`id_materia`);

--
-- Indexes for table `horario_asignado`
--
ALTER TABLE `horario_asignado`
  ADD PRIMARY KEY (`id_horario`,`id_usuario`),
  ADD KEY `FK_RELATIONSHIP_6` (`id_usuario`);

--
-- Indexes for table `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`id_materia`),
  ADD KEY `FK_RELATIONSHIP_4` (`id_nivel`);

--
-- Indexes for table `nivel`
--
ALTER TABLE `nivel`
  ADD PRIMARY KEY (`id_nivel`);

--
-- Indexes for table `paralelo`
--
ALTER TABLE `paralelo`
  ADD PRIMARY KEY (`id_paralelo`),
  ADD KEY `FK_RELATIONSHIP_5` (`id_nivel`);

--
-- Indexes for table `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `NOMBRE_USUARIO` (`nombre_usuario`),
  ADD KEY `FK_RELATIONSHIP_7` (`id_rol`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `horario`
--
ALTER TABLE `horario`
  MODIFY `id_horario` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `materia`
--
ALTER TABLE `materia`
  MODIFY `id_materia` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nivel`
--
ALTER TABLE `nivel`
  MODIFY `id_nivel` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paralelo`
--
ALTER TABLE `paralelo`
  MODIFY `id_paralelo` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rol`
--
ALTER TABLE `rol`
  MODIFY `id_rol` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `horario`
--
ALTER TABLE `horario`
  ADD CONSTRAINT `FK_RELATIONSHIP_3` FOREIGN KEY (`id_materia`) REFERENCES `materia` (`id_materia`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `horario_asignado`
--
ALTER TABLE `horario_asignado`
  ADD CONSTRAINT `FK_RELATIONSHIP_2` FOREIGN KEY (`id_horario`) REFERENCES `horario` (`id_horario`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `FK_RELATIONSHIP_6` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `materia`
--
ALTER TABLE `materia`
  ADD CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`id_nivel`) REFERENCES `nivel` (`id_nivel`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `paralelo`
--
ALTER TABLE `paralelo`
  ADD CONSTRAINT `FK_RELATIONSHIP_5` FOREIGN KEY (`id_nivel`) REFERENCES `nivel` (`id_nivel`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `FK_RELATIONSHIP_7` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id_rol`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
